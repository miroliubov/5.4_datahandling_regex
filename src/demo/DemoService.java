package src.demo;

import src.service.Numbers;
import src.service.StringService;
import src.service.Template;

public class DemoService implements IDemoService {

    @Override
    public void execute() {

        System.out.println("Задача 1");
        StringService stringService = new StringService();
        System.out.println(stringService.getRegEx("Ребе, Ви случайно не знаете, сколько тогда Иуда получил " +
                "по нынешнему курсу?"));

        System.out.println("\nЗадача 2, максимальная длина слова");
        System.out.println(stringService.maxString("Ребе, Ви случайно не знаете, сколько тогда Иуда получил " +
                "по нынешнему курсу?"));

        System.out.println("\nЗадача 2, минимальная длина слова");
        System.out.println(stringService.minString("Ребе, Ви случайно не знаете, сколько тогда Иуда получил " +
                "по нынешнему курсу?"));

        System.out.println("\nЗадача 3");
        Numbers numbers = new Numbers();
        System.out.println(numbers.getNumberWithoutPrefix("Дана строка, содержащая в себе, помимо прочего, " +
                "номера телефонов. Необходимо удалить из этой строки префиксы локальных номеров, соответствующих " +
                "Ижевску. Например, из \"+7 (3412) 517-647\" получить \"517-647\"; \"8 (3412) 4997-12\" > " +
                "\"4997-12\"; \"+7 3412 90-41-90\" > \"90-41-90\""));

        System.out.println("\nЗадача 4");
        Template template = new Template();
        System.out.println(template.replaceTemplate("Уважаемый, $userName, извещаем вас о том, что на вашем " +
                "счете $номерСчета скопилась сумма, превышающая стоимость $числоМесяцев месяцев пользования " +
                "нашими услугами. Деньги продолжают поступать. Вероятно, вы неправильно настроили автоплатеж. " +
                "С уважением, $пользовательФИО $должностьПользователя",
                "$userName, $номерСчета, $числоМесяцев, $пользовательФИО, $должностьПользователя",
                "Василий, 88005553535, 5, Васильев Василий Васильевич, Директор"));
    }
}
