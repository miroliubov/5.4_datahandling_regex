package src.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringService {

    public String getRegEx(String input) {
        Pattern p = Pattern.compile("[а-яА-Яa-zA-Z']+");
        Matcher m = p.matcher(input);
        StringBuilder stringBuilder = new StringBuilder();

        while ( m.find() ) {
            stringBuilder.append(input.substring(m.start(), m.end()).toLowerCase()).append('\n');
        }
        return stringBuilder.toString();
    }

    public String maxString(String input) {
        String[] strings = getRegEx(input).split("\n");
        String result = "";
        for (String s: strings) {
            if (s.length() > result.length()) {
                result = s;
            }
        }
        return  result;
    }

    public String minString(String input) {

        String[] strings = getRegEx(input).split("\n");
        String result = strings[0];
        for (String s: strings) {
            if (s.length() < result.length()) {
                result = s;
            }
        }
        return result;
    }
}
