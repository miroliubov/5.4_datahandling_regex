package src.service;

public class Template {

    public String replaceTemplate(String input, String templateKey, String templateValue) {
        String[] templateKeys = templateKey.split(", ");
        String[] templateValues = templateValue.split(", ");

        for (int i = 0; i < templateKeys.length; i++) {
            input = input.replace(templateKeys[i], templateValues[i]);
        }

        return input;
    }
}
